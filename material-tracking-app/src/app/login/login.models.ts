import { AbstractResponse } from 'app/core/model/http/abstract-response.model';


export class LoginRequest {

  public username: string;
  public password: string;

  constructor(

  ) { }
}

export class LoginResponse extends AbstractResponse {

  public success: boolean;

  constructor() {
    super();
  }
}

export class LogoutRequest {

  constructor() { }
}

export class LogoutResponse extends AbstractResponse {

  public success: boolean;

  constructor() {
    super();
  }
}
