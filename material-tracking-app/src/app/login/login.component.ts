import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'app/login/login.service';
import { LoginResponse, LoginRequest } from 'app/login/login.models';

import { SessionHelper } from 'app/core/session.helper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMessageFlag = false;
  errorMessage: string;

  @Input()
  request: LoginRequest;

  @Input()
  response: LoginResponse;

  loginForm: FormGroup;

  constructor(
    private loginService: LoginService,
    private sessionHelper: SessionHelper,
    private formBuilder: FormBuilder,
    private router: Router
  ) {

    this.request = new LoginRequest();
    this.sessionHelper.setAuthInfo(null, null);

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  login() {
    this.request = this.loginForm.getRawValue();
    this.loginService.login(this.request).subscribe(res => {

        if (res.success === true) {
          this.sessionHelper.setAuthInfo(res.token, res.user);
          this.router.navigate(['/officer']);
        } else {
          this.errorMessageFlag = true;
          if (res.success === true) {
            this.errorMessage = 'You do not have permission to login. Please contact your administrator.';
          } else {
            this.errorMessage = res.message;
          }
        }
      },
      err => {
        console.log(err);
      }

    );

  }

}
