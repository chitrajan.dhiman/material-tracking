import { Injectable, Inject } from '@angular/core';

import { HttpRestClient } from 'app/core/http.client';
import { LoginRequest } from './login.models';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpRestClient) { }

  login(request: LoginRequest) {
    return this.httpClient.postData('login', request);
  }
}
