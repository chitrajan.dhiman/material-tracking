import { Injectable, Inject } from '@angular/core';

import { HttpRestClient } from 'app/core/http.client';

import { LogoutRequest } from 'app/login/login.models';

@Injectable({
    providedIn: 'root'
})
export class LogoutService {

    constructor(private httpClient: HttpRestClient) {
    }

    logout(request: LogoutRequest) {
        return this.httpClient.postData('logout', request);
    }
}
