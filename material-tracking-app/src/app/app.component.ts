import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor() { }

  ngOnInit() { }

  ngAfterViewInit() { }

  ngOnDestroy() { }

}
