import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Mode } from 'app/core/model/core.enum';

@Component({
  selector: 'app-material-manage',
  templateUrl: './material-manage.component.html',
  styleUrls: ['./material-manage.component.scss']
})
export class MaterialManageComponent implements OnInit {

  constructor(
    private route: ActivatedRoute
  ) { }

  ticketId: number = null;
  Mode = Mode;

  ngOnInit() {

    const ticketId = this.route.snapshot.params.id;

    if (ticketId) {
      this.ticketId = ticketId;
    }
  }

}
