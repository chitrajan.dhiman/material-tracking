import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpRestClient } from 'app/core/http.client';
import { Ticket } from 'app/core/model/material.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  constructor(
    private httpClient: HttpRestClient
  ) { }

  getMaterialTypes(): Observable<any> {
    return this.httpClient.getData('assets/data/materialTypes.json');

  }

  getStockSites(): Observable<any> {
    return this.httpClient.getData('assets/data/stockSites.json');
  }

  getEquipmentLoads(): Observable<any> {
    return this.httpClient.getData('assets/data/equipmentLoadTypes.json');
  }

  getTickets(): Observable<any> {
    return this.httpClient.getData('assets/data/tickets.json');
  }

  getTicketById(ticketId: number): Observable<any> {

    return this.getTickets().pipe(
      map(res => {
        const requestedTicket = res.find((ticket: Ticket) => ticket.ticketId == ticketId);
        return requestedTicket;
      })
    );
  }
  save(ticket: Ticket): Observable<any> {

    return of(true);
  }

  update(ticket: Ticket): Observable<any> {

    return of(true);
  }
}
