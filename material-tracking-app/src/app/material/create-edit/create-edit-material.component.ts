import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, AbstractControl, ValidatorFn, Validators, FormControl } from '@angular/forms';
import { Ticket, MaterialLoad, SubmitAction } from 'app/core/model/material.model';
import { MaterialService } from '../material.service';
import { ToastService } from 'app/core/service/toast.service';
import { Mode } from 'app/core/model/core.enum';
import { Observable, of } from 'rxjs';
@Component({
  selector: 'app-create-edit-material',
  templateUrl: './create-edit-material.component.html',
  styleUrls: ['./create-edit-material.component.scss']
})
export class MaterialCreateEditComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private materialService: MaterialService,
    private toastService: ToastService
  ) { }

  Mode = Mode;

  @Input()
  ticketId: number;

  @Input()
  mode: Mode = Mode.CREATE;

  form: FormGroup = this.formBuilder.group({
    ...new Ticket(),
    loads: this.formBuilder.array([
      this.formBuilder.group(new MaterialLoad())
    ])
  });

  materialTypes: Array<any> = null;
  equipmentLoadTypes: Array<any> = null;
  stockSites: Array<any> = null;

  get loads() {
    const loads = this.form.get('loads') as FormArray;
    return loads.controls;
  }

  ngOnInit() {

    if (this.ticketId) {
      this.getTicketById(this.ticketId);
    }

    this.getMetaData();
  }

  getTicketById(ticketId: number = this.ticketId) {
    this.materialService.getTicketById(ticketId).subscribe(res => {
      this.mapTicket(res);
    });
  }

  getMetaData() {

    this.getMaterialTypes();
    this.getStockSites();
    this.getEquipmentLoadTypes();
  }

  mapTicket(ticket: Ticket) {
    console.log(this.form);
    this.form.patchValue(ticket);
    this.form.markAsPristine();

    const loadsData: Array<MaterialLoad> = ticket.loads;
    const loads: FormArray = this.form.get('loads') as FormArray;

    loadsData.forEach(load => {

      if (loads.length < loadsData.length) {

        const group = this.formBuilder.group(new MaterialLoad());
        group.patchValue(load);
        loads.push(group);
      }

    });

    this.disableFormFields();
  }

  getMaterialTypes() {

    this.materialService.getMaterialTypes()
      .subscribe(data => {
        this.materialTypes = data;
      });
  }

  getStockSites() {

    this.materialService.getStockSites()
      .subscribe(data => {
        this.stockSites = data;
      });
  }

  getEquipmentLoadTypes() {

    this.materialService.getEquipmentLoads()
      .subscribe(data => {
        this.equipmentLoadTypes = data;
      });
  }

  submit(submitAction: SubmitAction = SubmitAction.SUBMITTED) {

    const ticket: Ticket = this.form.getRawValue() as Ticket;
    this.validate()
      .subscribe(
        valid => {

          if (valid) {

            if (ticket.ticketId) {

              this.materialService.update(ticket)
                .subscribe(response => {
                  this.saveCallBack(response, submitAction);
                });
            } else {

              this.materialService.save(ticket)
                .subscribe(response => {
                  this.saveCallBack(response, SubmitAction.SUBMITTED, true);

                });

            }
          }
        }
      );
  }

  saveCallBack(response: any, submitAction: SubmitAction = SubmitAction.SUBMITTED, saveAction: boolean = false) {
    if (response.success || response) {
      if (saveAction) {
        this.toastService.showSuccess(`Material Ticket Successfully ${SubmitAction.SUBMITTED}`);

      } else {
        this.toastService.showSuccess(`Material Ticket Successfully ${submitAction}.`);

      }
    }

  }

  validateFields(validators: Map<string, Array<ValidatorFn>>, formControl: AbstractControl): boolean {

    let formArray: FormArray = null;

    if (formControl instanceof FormArray) {

      formArray = formControl as FormArray;
      return formArray.controls.reduce((acc, val) => this.validateFields(validators, val), true);
    }

    const controls: Array<any> = new Array<any>();

    Array.from(validators.keys()).forEach(key => {

      const control = formControl.get(key);
      control.setValidators(validators.get(key));
      control.markAsTouched();
      control.updateValueAndValidity({ emitEvent: false });
      controls.push(control);
    });

    if (controls.reduce((accoumulator, value) =>
      accoumulator && !value.invalid, true
    )) {
      return true;
    }
    return false;
  }

  validate(): Observable<any> {

    if (!this.validateFormControlArray(this.form.get('loads')) || !this.validateFields(this.getCommonFieldValidators(), this.form)) {

      this.toastService.showError('Please fill required fields.');
      return of(false);
    }

    return of(true);
  }

  validateFormControlArray(formControl: AbstractControl, minLenght: number = 1): boolean {

    const values: Array<MaterialLoad> = formControl.value;

    if (values && values.length >= minLenght && this.validateFields(this.getMaterialFieldValidators(), formControl)) {
      return true;
    }

    return false;
  }

  getCommonFieldValidators(): Map<string, Array<ValidatorFn>> {

    const requiredFields: Array<string> = ['operator', 'date', 'supervisor', 'truckNumber'];
    const map: Map<string, Array<ValidatorFn>> = new Map<string, Array<ValidatorFn>>();

    if (true) {
      requiredFields.forEach(field =>
        map.set(field, [Validators.required]));
    }
    return map;
  }

  getMaterialFieldValidators(): Map<string, Array<ValidatorFn>> {

    const requiredFields: Array<string> = ['materialType', 'stockSite', 'equipmentLoadCode', 'operation'];
    const map: Map<string, Array<ValidatorFn>> = new Map<string, Array<ValidatorFn>>();

    if (true) {
      requiredFields.forEach(field =>
        map.set(field, [Validators.required]));
    }
    return map;
  }

  addLoad() {

    const loads: FormArray = this.form.get('loads') as FormArray;
    loads.push(this.formBuilder.group(new MaterialLoad()));
  }

  deleteLoad(index: number) {

    const loads: FormArray = this.form.get('loads') as FormArray;
    loads.removeAt(index);
  }

  disableFormFields() {

    Object.keys(this.form.controls).forEach(key => {
      this.form.controls[key].disable();
    });
  }
}
