import { Component, OnInit } from '@angular/core';
import { Ticket } from 'app/core/model/material.model';
import { Router } from '@angular/router';
import { tick } from '@angular/core/testing';
import { MaterialService } from '../material.service';
@Component({
  selector: 'app-material-ticket-list',
  templateUrl: './material-ticket-list.component.html',
  styleUrls: ['./material-ticket-list.component.scss']
})
export class MaterialTicketListComponent implements OnInit {

  data: Array<Ticket> = null;
  displayedColumns: string[] = ['ticketId', 'operator', 'supervisor', 'date', 'truckNumber'];

  constructor(
    private materialService: MaterialService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getTickets();
  }

  redirectToMange(ticket: Ticket) {
    this.router.navigate(['material-ticket', 'manage', ticket.ticketId]);
  }

  getTickets() {
    this.materialService.getTickets().subscribe(
      response => this.data = response
    );
  }
}
