import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { LoaderService } from '../../core/service/loader.service';
import { LoaderState } from '../../core/model/core.model';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-loader',
    templateUrl: 'loader.component.html',
})
export class LoaderComponent implements OnInit {
    show = false;
    private subscription: Subscription;
    constructor(
        private loaderService: LoaderService
    ) {

    }
    ngOnInit() {
        this.subscription = this.loaderService.loaderState
            .subscribe((state: LoaderState) => {
                if (state.counter == 0) {
                    this.show = false;
                }
                else {
                    this.show = true;
                }


            });


    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
