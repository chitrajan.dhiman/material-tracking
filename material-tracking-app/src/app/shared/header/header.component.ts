import { Component, OnInit, OnDestroy } from '@angular/core';
import { SessionHelper } from 'app/core/session.helper';
import { SessionInfo } from 'app/core/model/session-info.model';
import { Subscription } from 'rxjs';
import { AuthenticationState } from 'app/core/model/core.model';
import { Router } from '@angular/router';
import { LogoutService } from 'app/login/logout.service';
import { LogoutRequest } from 'app/login/login.models';
import { ToastService } from 'app/core/service/toast.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  isAuthenticated = false;
  isGuest = false;

  menuItems: any[] = [];
  request = new LogoutRequest();

  constructor(
    private sessionHelper: SessionHelper,
    private router: Router,
    private logoutService: LogoutService,
    private toastService: ToastService
  ) { }

  ngOnInit() {

    const sessionInfo: SessionInfo = this.sessionHelper.getUser();
    if (sessionInfo) {
      this.isAuthenticated = true;
    }
    this.renderMenu();

    this.subscription = this.sessionHelper.authenticationState
      .subscribe((state: AuthenticationState) => {
        if (state.authenticated === true) {
          this.isAuthenticated = true;
        } else {
          this.isAuthenticated = false;
          this.isGuest = false;
        }
        this.renderMenu();
      });
  }

  renderMenu() {
    this.menuItems = [];



    this.menuItems.push(
      {
        label: 'Material Ticket',
        type: 'route',
        link: 'material-ticket'
      });
  }

  logout() {
    this.logoutService.logout(this.request).subscribe(res => {
      if (res.success === true) {
        this.sessionHelper.removeAll();
        this.router.navigate(['/']);
      } else {
        this.toastService.showError('Couldn\'t complete the request. Try again later');
      }
    },
      err => {
        console.log(err);
      }

    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
