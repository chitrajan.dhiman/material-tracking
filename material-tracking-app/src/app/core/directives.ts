import { Directive } from '@angular/core';
import { Input } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { ViewContainerRef } from '@angular/core';

import { AuthorizationGaurd } from './auth.guard';

@Directive({ selector: '[hasAllPermissions]' })
export class HasAllPermissionsDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authorizationGaurd: AuthorizationGaurd
  ) { }

  @Input() set hasAllPermissions(permissionsString: String) {

    var permissionArray = permissionsString.split(',');

    var hasPermission = this.authorizationGaurd.hasAllPermissions(permissionArray);

    if (hasPermission) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }
}

@Directive({ selector: '[hasAnyPermissions]' })
export class HasAnyPermissionsDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authorizationGaurd: AuthorizationGaurd
  ) { }

  @Input() set hasAnyPermissions(permissionsString: String) {

    var permissionArray = permissionsString.split(',');

    var hasPermission = this.authorizationGaurd.hasAllPermissions(permissionArray);

    if (hasPermission) {
      // If condition is true add template to DOM
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      // Else remove template from DOM
      this.viewContainer.clear();
    }
  }
}
