import { Injectable } from '@angular/core';

import { LocalStorageService } from 'angular-2-local-storage';
import { SessionInfo } from './model/session-info.model';
import { AuthenticationState } from './model/core.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionHelper {

  private authenticationSubject = new Subject<AuthenticationState>();
  authenticationState = this.authenticationSubject.asObservable();

  constructor(
    private storage: LocalStorageService
  ) { }

  set(key: string, value: any) {
    this.storage.set(key, value);
  }

  get<T>(key: string): T {
    const item = this.storage.get(key);
    if (!item || item === 'undefined' || item == null) {
      return null;
    } else {
      return item as T;
    }
  }


  setAuthInfo(token: string, user: SessionInfo) {
    if (user == null) {
      this.storage.set('user', null);
      this.storage.set('token', null);
      this.updateAuthenticationState(false);
    } else {
      this.storage.set('user', user);
      this.storage.set('token', token);
      this.updateAuthenticationState(true);
    }
  }

  updateAuthenticationState(authenticated: boolean) {
    this.authenticationSubject.next({ authenticated} as AuthenticationState);
  }

  getUser(): SessionInfo {
    const userJson = this.storage.get('user');
    let user = null;
    if (userJson !== 'undefined' && userJson != null) {
      user = userJson;
    }
    return user;
  }

  getToken(): SessionInfo {
    return this.storage.get('token');
  }



  hasPermission(permission: string) {
    let hasPermission = false;
    const userPermissions = this.getUser().authorities;
    if (userPermissions.filter(pri => pri.toUpperCase() === permission.toUpperCase()).length > 0) {
      hasPermission = true;
    }

    return hasPermission;
  }

  hasAnyPermission(permissions: Array<string>) {
    let hasPermission = false;
    const userPermissions = this.getUser().authorities;

    for (let i = 0; i < permissions.length; i++) {

      const permission = permissions[i];
      if (userPermissions.filter(pri => pri.toUpperCase() === permission.toUpperCase()).length > 0) {
        hasPermission = true;
        break;
      }
    }

    return hasPermission;
  }

  hasAllPermission(permissions: Array<string>) {
    let hasPermission = false;
    const userPermissions = this.getUser().authorities;

    for (let i = 0; i < permissions.length; i++) {

      const permission = permissions[i];

      if (userPermissions.filter(pri => pri.toUpperCase() === permission.toUpperCase()).length > 0) {
        hasPermission = true;
      } else {
        hasPermission = false;
        break;
      }
    }

    return hasPermission;
  }

  removeAll() {
    this.storage.clearAll();
    this.updateAuthenticationState(false);
  }

  removeBykey(key: string) {
    this.storage.remove(key);
  }

}
