import { Injectable } from '@angular/core';
import { HttpRestClient } from '../http.client';
import { UiUser } from '../model/user.model';

@Injectable()
export class UserService {

  constructor( private httpClient:HttpRestClient) { }

  saveGuestUser(bean : UiUser){
    return this.httpClient.postData('guest-user', bean);
  }

  signUp(bean : UiUser){
    return this.httpClient.postData('sign-up', bean);
  }
}
