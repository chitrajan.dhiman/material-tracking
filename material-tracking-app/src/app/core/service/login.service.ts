import { Injectable } from '@angular/core';
import { HttpRestClient } from '../http.client';
import { UserLoginRequest } from '../model/http/user-login-request';
import { UserAcountResetOrActivateRequest } from '../model/http/user-acount-reset-or-activate-request';

@Injectable()
export class LoginService {

  constructor(private httpClient: HttpRestClient) { }

  login(bean: UserLoginRequest) {
    return this.httpClient.postData('login', bean);
  }

  sessionHeartBeat() {
    return this.httpClient.getData('heart-beat');
  }

  initiatingPasswordReset(bean: UserAcountResetOrActivateRequest) {
    return this.httpClient.putData('user/reset-password-request', bean);
  }

  resetPassword(bean: UserAcountResetOrActivateRequest) {
    return this.httpClient.putData('user/reset-password', bean);
  }
}
