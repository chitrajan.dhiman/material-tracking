import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class ToastService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  show(message, duration, cssClass) {
    this.snackBar.open(message, '', {
      duration: duration,
      panelClass: cssClass,
    });
  }

  showSuccess(message) {
    this.show(message, 5000, 'success');
  }

  showError(message) {
    if (message) {
      this.show(message, 5000, 'error');
    } else {
      this.showError('There was some error while processing the request');
    }
  }

  showWarning(message) {
    this.show(message, 5000, 'warning');
  }

  postDeletedToast(entity) {
    const message = "Selected " + entity + " has been deleted";
    this.showSuccess(message);
  }

  postSavedToast(entity, isUpdate) {
    const message = entity + " has been " + (isUpdate ? "Updated." : "Created.");
    this.showSuccess(message);
  }
}
