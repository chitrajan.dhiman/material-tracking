import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

  LOADER_COUNTER: number;
 
  constructor() {
    this.LOADER_COUNTER = 0;
  }

  getCounter() {
    return this.LOADER_COUNTER;
  }

  incrementCounter() {
    this.LOADER_COUNTER++;
  }

  decrementCounter() {
    if (this.LOADER_COUNTER > 0) {
      this.LOADER_COUNTER--;
    }
  }
}
