import { Injectable } from '@angular/core';
import { LoaderState } from '../model/core.model';

import { Subject } from 'rxjs';
import { GlobalService } from './global.service';

@Injectable()
export class LoaderService {

  private loaderSubject = new Subject<LoaderState>();
  loaderState = this.loaderSubject.asObservable();

  constructor(private globalService:GlobalService) { }

  increment() {
    this.globalService.incrementCounter();
    this.loaderSubject.next(<LoaderState>{ counter: this.globalService.getCounter() });
  }
  decrement() {
    this.globalService.decrementCounter();
    this.loaderSubject.next(<LoaderState>{ counter: this.globalService.getCounter() });
  }

}
