import { HTTP_INTERCEPTORS } from "@angular/common/http";

import { UrlInterceptor } from "./url-interceptor";
import { AuthInterceptor } from "./auth-interceptor";
import { ParamsInterceptor } from "./params-interceptor";
import { LoadingInterceptor } from "./loading-interceptor";
import { ErrorInterceptor } from "./error-interceptor";

export const HttpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: UrlInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ParamsInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
];
