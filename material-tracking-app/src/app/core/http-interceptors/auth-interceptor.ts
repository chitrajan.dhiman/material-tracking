import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor
} from '@angular/common/http';
import { SessionHelper } from '../session.helper';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private sessionHelper: SessionHelper) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    if (this.sessionHelper.get('token') != null) {

      return next.handle(
        req.clone({
          headers: req.headers.append('Authorization', `Bearer ${this.sessionHelper.getToken()}`)
          .append('Strict-Transport-Security', 'max-age=63072000; includeSubdomains; preload')
        })
      );
    } else {
      return next.handle(
        req.clone({
          headers: req.headers.append('Strict-Transport-Security', 'max-age=63072000; includeSubdomains; preload')
        })
      );
    }
  }
}
