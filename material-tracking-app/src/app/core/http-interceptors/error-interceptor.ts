import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';

import { catchError } from 'rxjs/operators';
import * as _ from 'lodash';
import { ToastService } from '../service/toast.service';
import { SessionHelper } from '../session.helper';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private toastService: ToastService,
    private router: Router,
    private sessionHelper: SessionHelper) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError(
        (res): Observable<any> => {
          if (res.status === 0) {
            this.sessionHelper.removeAll();
            return throwError(res);
          }

          if (res.status === 401) {
            this.sessionHelper.removeAll();
            this.toastService.showError(res.message || 'You are not authorized. Please Login.');
            return throwError(res);
          }

          if (res.status === 403) {
            this.router.navigate(['un-authorized']);
            return throwError(res);
          }
        }
      )
    );
  }
}
