import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpEvent
} from '@angular/common/http';
import { finalize} from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SessionHelper } from '../session.helper';
import { Router } from '@angular/router';
import { LoaderService } from '../service/loader.service';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  private enableLoad = new Subject<any>();
  private disableLoad = new Subject<any>();

  // Observable string streams
  numberOfAjaxCalls = 0;
  enableLoading$ = this.enableLoad.asObservable();
  disableLoading$ = this.disableLoad.asObservable();

  constructor(
      private loaderService: LoaderService,
      private sessionHelper: SessionHelper,
      private router: Router
      ) {

       }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const view = this;
      this.showSpinner();
      return next.handle(req).pipe(
          finalize(() => view.hideSpinner())
      );
  }


showSpinner() {
  this.enableLoad.next();
  this.numberOfAjaxCalls = this.numberOfAjaxCalls + 1;
  this.loaderService.increment();
}

hideSpinner() {
  this.loaderService.decrement();
}
}
