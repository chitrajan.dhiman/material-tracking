import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpParams
} from '@angular/common/http';

@Injectable()
export class ParamsInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(
      req.clone({
        url: encodeURI(
          req.url
            .split('&')
            .filter(item => !!item.split('=')[1])
            .join('&')
        ),
        params: req.params
          .keys()
          .map(key => ({
            key,
            value: req.params.get(key)
          }))
          .filter(item => !!item.value)
          .reduce(
            (params, item) => params.set(item.key, item.value),
            new HttpParams()
          )
      })
    );
  }
}
