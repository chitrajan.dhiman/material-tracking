import { AbstractUiBean } from './abstract-ui-bean.model';

export class UiUser extends AbstractUiBean{
	email:string;
	password:string;
	firstName:string;
	lastName:string;
	phoneNumber:string;

	userRoleId: string;
}