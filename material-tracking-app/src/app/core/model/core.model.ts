export interface LoaderState {
    counter: number;
}

export interface ParticlesState {
    isVisible : boolean;
}

export interface AuthenticationState {
    authenticated: boolean;
}