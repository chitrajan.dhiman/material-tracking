export class AbstractUiBean{
	
	createdById:string;
	createdByName:string;
	creationDatetime:Date;
	
	
	lastModifiedById:string;
	lastModifiedByName:string;
	lastModifiedDatetime:Date; 
	
	guid:string; 
}