export class AbstractSearchCriteria{
	
	selectedIds:number[];
	selectedGuids:string[];
	
	searchKeyword:string;
	searchField:string;

	fromDate:Date;
	toDate:Date;
	
	sortBy:string;
	sortOrder:string;

	pageNumber:number;
	resultPerPage:number;

	resultType:string;
}