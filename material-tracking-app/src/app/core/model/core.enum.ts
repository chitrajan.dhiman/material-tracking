export enum Mode {
    CREATE = 'CREATE',
    MANAGE = 'MANAGE',
    RENEW = 'RENEW',
    VIEW = 'VIEW'
}

export enum View {
    USER = 'USER',
    OFFICER = 'OFFICER'
}
