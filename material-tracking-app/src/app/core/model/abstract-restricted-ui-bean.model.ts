import { AbstractUiBean } from './abstract-ui-bean.model';
import { UiUser } from './user.model';

export class AbstractRestrictedUiBean extends AbstractUiBean{
	
	userId:string;
	user:UiUser;

}