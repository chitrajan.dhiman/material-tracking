import { AbstractResponse } from './abstract-response.model';

export class AbstractResponseBean<T> extends AbstractResponse {

object: T;
objects: T[];
objectId: string;

totalResultsCount: number;
resultCountPerPage: number;
currentPageIndex: number;

}
