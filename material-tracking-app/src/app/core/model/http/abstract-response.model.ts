export class AbstractResponse{
	success:boolean;
	message:string;
	errorMap:Map<String, String>;
}