import { AbstractRequest } from './abstract-request.model';

export class UserLoginRequest extends AbstractRequest{
	username:string = null;
	password:string = null;
}