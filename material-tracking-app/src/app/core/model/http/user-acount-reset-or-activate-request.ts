export class UserAcountResetOrActivateRequest{
	email:string = null;
	password:string = null;
	emailCommunicationToken:string = null;
}