export class Ticket {

    ticketId: number = null;
    operator: string = null;
    supervisor: string = null;
    truckNumber: number = null;
    date: Date = null;
    loads: Array<MaterialLoad> = [];
}

export class MaterialLoad {

    materialType: string = null;
    stockSite: string = null;
    equipmentLoadCode: string = null;
    operation: string = null;
}

export enum SubmitAction {
    UPDATED = 'updated',
    SUBMITTED = 'submitted'
}