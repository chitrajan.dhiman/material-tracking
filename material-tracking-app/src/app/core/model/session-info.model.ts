export class SessionInfo{

	public guid: String;
	public name: String;
	public username: String;
	public authorities: Array<String>;
	public sessionToken: String;
	public accountExpiryDate : Date;
}
