import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot } from '@angular/router';
import { RouterStateSnapshot } from '@angular/router';

import { SessionHelper } from 'app/core/session.helper';

@Injectable()
export class AuthorizationGaurd {

  constructor(private sessionHelper: SessionHelper) {

  }

  hasAllPermissions(permissionArray: Array<string>): boolean {

    var hasPermission = false;

    if (permissionArray.length > 1) {
      hasPermission = this.sessionHelper.hasAllPermission(permissionArray);
    }
    else {
      hasPermission = this.sessionHelper.hasPermission(permissionArray[0]);
    }

    return hasPermission;
  }

  hasAnyPermissions(permissionArray: Array<string>): boolean {

    var hasPermission = false;

    if (permissionArray.length > 1) {
      hasPermission = this.sessionHelper.hasAnyPermission(permissionArray);
    }
    else {
      hasPermission = this.sessionHelper.hasPermission(permissionArray[0]);
    }

    return hasPermission;
  }

};

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(private router: Router, private sessionHelper: SessionHelper, private authorizationGaurd: AuthorizationGaurd) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    let authenticated: boolean = false;
    let authorized: boolean = false;

    if (this.sessionHelper.getUser()) {
      authenticated = true;

      if (this.checkHasAllPermissions(route) && this.checkHasAnyPermissions(route)) {
        authorized = true;
      }
    }

    if (authenticated == false) {
      this.router.navigate(['/']);
      return false;
    }
    else if (authorized == false) {
      this.router.navigate(['/authorization-error']);
      return false;
    }
    else {
      return true;
    }

  }

  checkHasAllPermissions(route: ActivatedRouteSnapshot): boolean {
    let accessAllowed: boolean = false;

    let requiredPermissionsArray = route.data["hasAllPermissions"] as Array<string>;

    if (requiredPermissionsArray != null) {
      let requiredPermissions = requiredPermissionsArray[0].split(',');
      accessAllowed = this.authorizationGaurd.hasAllPermissions(requiredPermissions);
    }
    else {
      accessAllowed = true;
    }

    return accessAllowed;
  }

  checkHasAnyPermissions(route: ActivatedRouteSnapshot) {
    let accessAllowed: boolean = false;

    let requiredPermissionsArray = route.data["hasAnyPermissions"] as Array<string>;

    if (requiredPermissionsArray != null) {
      let requiredPermissions = requiredPermissionsArray[0].split(',');
      accessAllowed = this.authorizationGaurd.hasAnyPermissions(requiredPermissions);
    }
    else {
      accessAllowed = true;
    }

    return accessAllowed;
  }
};
