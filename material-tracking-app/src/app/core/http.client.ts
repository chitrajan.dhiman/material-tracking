import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { AbstractResponse } from './model/http/abstract-response.model';
import { AbstractResponseBean } from './model/http/abstract-response-bean.model';

@Injectable({
  providedIn: 'root'
})
export class HttpRestClient {

  constructor(
    private httpClient: HttpClient
  ) {
  }

   createHeaderForMultipartRequest() {
     let headers = new HttpHeaders({ Accept: 'application/json' });
    //  headers = headers.append('responseType', 'blob');

     return headers;
   }

   createHeaderForRestRequest() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return headers;
  }


  getData(url: string): Observable<AbstractResponseBean<any>> {

    return this.httpClient.get<AbstractResponseBean<any>>(url, {
       withCredentials: true
    });
  }

  getFile(url: string): Observable<HttpResponse<string>> {

    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'text/plain');

    return this.httpClient.get(url, {
      headers,
      observe: 'response',
      responseType: 'text'
    });
  }

  search(url: string, params: any) {

     return this.httpClient.get<any>(url, {
        withCredentials: true,
        params
     });
  }

  postData(url: string, data: any) {

    const headers = this.createHeaderForRestRequest();

    return this.httpClient.post<any>(url, JSON.stringify(data), {
      headers,
      withCredentials: true
    });
  }

  putData(url: string, data: any) {

    const headers = this.createHeaderForRestRequest();

    return this.httpClient.put<any>(url, JSON.stringify(data), {
      headers,
      withCredentials: true
    });
  }

  deleteData(url: string): Observable<AbstractResponse> {

    const headers = this.createHeaderForRestRequest();

    return this.httpClient.delete<AbstractResponse>(url, {
      headers,
      withCredentials: true
    });
  }

  postMutlipartData(url: string, formData: FormData) {
    const headers = this.createHeaderForMultipartRequest();

    return this.httpClient.post<any>(url, formData, {
      headers,
      withCredentials: true
    });
  }

  putMutlipartData(url: string, formData: FormData) {

    return this.httpClient.put<any>(url, formData, {
       withCredentials: true
    });
  }
}
