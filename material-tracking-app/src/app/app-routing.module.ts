import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { MaterialCreateEditComponent } from './material/create-edit/create-edit-material.component';
import { MaterialTicketListComponent } from './material/list/material-ticket-list.component';
import { MaterialManageComponent } from './material/manage/material-manage.component';
import { MaterialComponent } from './material/material.component';


const routes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: 'landing', component: LandingComponent },
  {
    path: 'material-ticket', component: MaterialComponent
    , children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: MaterialTicketListComponent },
      { path: 'create', component: MaterialCreateEditComponent },
      { path: 'manage/:id', component: MaterialManageComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
