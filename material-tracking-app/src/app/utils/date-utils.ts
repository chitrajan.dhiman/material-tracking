
export function appendCurrentTimeInDate(givenDate: Date) {

    const currentDate = new Date();

    givenDate.setHours(currentDate.getHours());
    givenDate.setMinutes(currentDate.getMinutes());
    givenDate.setSeconds(currentDate.getSeconds());

    return givenDate;
}
