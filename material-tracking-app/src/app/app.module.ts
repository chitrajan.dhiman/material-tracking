import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LocalStorageModule } from 'angular-2-local-storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatRadioModule,
  MatCardModule,
  MatDatepickerModule,
  MatMenuModule,
  MatSidenavModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule,
  MatPaginatorModule,
  MatIconModule,
  MatNativeDateModule,
  MatButtonToggleModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTooltipModule,
  MatSelectModule,
  MatTabsModule,
  MatAccordion,
  MatExpansionModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { HasAllPermissionsDirective, HasAnyPermissionsDirective } from './core/directives';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AuthorizationGaurd, AuthenticationGuard } from './core/auth.guard';
import { LoaderComponent } from './shared/loader/loader.component';
import { GlobalService } from './core/service/global.service';
import { HttpInterceptorProviders } from './core/http-interceptors';
import { DeleteConfirmationDialog } from './shared/delete-confirmation.component';
import { RequiredTag } from './shared/required/required';
import { LoaderService } from './core/service/loader.service';
import { ToastService } from './core/service/toast.service';
import { UserService } from './core/service/user.service';
import { AbstactService } from './core/service/abstract-service.service';
import { LoginService } from './core/service/login.service';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { MaterialCreateEditComponent } from './material/create-edit/create-edit-material.component';
import { MaterialTicketListComponent } from './material/list/material-ticket-list.component';
import { MaterialManageComponent } from './material/manage/material-manage.component';
import { MaterialComponent } from './material/material.component';

@NgModule({
  declarations: [
    LoaderComponent,
    AppComponent,
    HasAllPermissionsDirective,
    HasAnyPermissionsDirective,
    HeaderComponent,
    FooterComponent,
    DeleteConfirmationDialog,
    RequiredTag,
    LandingComponent,
    LoginComponent,
    MaterialCreateEditComponent,
    MaterialTicketListComponent,
    MaterialManageComponent,
    MaterialComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatMenuModule,
    MatSidenavModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatStepperModule,
    MatSelectModule,
    MatTabsModule,
    MatExpansionModule,

    LocalStorageModule.forRoot({
      prefix: 'MaterialTrackinging-app',
      storageType: 'localStorage'
    })
  ],
  providers: [
    AuthorizationGaurd,
    AuthenticationGuard,
    LoaderService,
    GlobalService,
    ToastService,
    UserService,
    AbstactService,
    LoginService,
    HttpInterceptorProviders
  ],
  entryComponents: [DeleteConfirmationDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
